# Translation of ktexteditorpreviewplugin.pot into esperanto.
# Copyright (C) 2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the kate package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-08 00:49+0000\n"
"PO-Revision-Date: 2023-03-12 21:39+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ktexteditorpreviewview.cpp:31
#, kde-format
msgid "Preview"
msgstr "Antaŭrigardo"

#: previewwidget.cpp:45
#, kde-format
msgid "Lock Current Document"
msgstr "Ŝlosi Nunan Dokumenton"

#: previewwidget.cpp:46
#, kde-format
msgid "Lock preview to current document"
msgstr "Ŝlosi antaŭrigardon al aktuala dokumento"

#: previewwidget.cpp:47
#, kde-format
msgid "Unlock Current View"
msgstr "Malŝlosi Nunan Vidon"

#: previewwidget.cpp:47
#, kde-format
msgid "Unlock current view"
msgstr "Malŝlosi nunan vidon"

#: previewwidget.cpp:54
#, kde-format
msgid "Automatically Update Preview"
msgstr "Aŭtomate Ĝisdatigi Antaŭrigardon"

#: previewwidget.cpp:55
#, kde-format
msgid "Enable automatic updates of the preview to the current document content"
msgstr ""
"Ebligi aŭtomatajn ĝisdatigojn de la antaŭrigardo al la aktuala dokumentenhavo"

#: previewwidget.cpp:56
#, kde-format
msgid "Manually Update Preview"
msgstr "Mane Ĝisdatigi Antaŭrigardon"

#: previewwidget.cpp:58
#, kde-format
msgid ""
"Disable automatic updates of the preview to the current document content"
msgstr ""
"Malebligi aŭtomatajn ĝisdatigojn de la antaŭrigardo al la nuna dokumentenhavo"

#: previewwidget.cpp:63
#, kde-format
msgid "Update Preview"
msgstr "Ĝisdatigi Antaŭrigardon"

#: previewwidget.cpp:64
#, kde-format
msgid "Update the preview to the current document content"
msgstr "Ĝisdatigi la antaŭrigardon al la aktuala dokumentenhavo"

#: previewwidget.cpp:72
#, kde-format
msgid "View"
msgstr "Vido"

#: previewwidget.cpp:97
#, kde-format
msgid "No preview available."
msgstr "Neniu antaŭrigardo disponebla."

#: previewwidget.cpp:233
#, kde-format
msgid "About %1"
msgstr "Pri %1"
