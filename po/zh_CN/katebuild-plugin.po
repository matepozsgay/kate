msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-24 00:49+0000\n"
"PO-Revision-Date: 2023-05-22 14:00\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kate/katebuild-plugin.pot\n"
"X-Crowdin-File-ID: 5308\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE China, Guo Yunhe, Tyson Tan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-china@kde.org, i@guoyunhe.me, tysontan@tysontan.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "输出"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "再次构建"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "取消"

#: plugin_katebuild.cpp:194 plugin_katebuild.cpp:201 plugin_katebuild.cpp:1170
#, kde-format
msgid "Build"
msgstr "构建"

#: plugin_katebuild.cpp:204
#, kde-format
msgid "Select Target..."
msgstr "选择目标..."

#: plugin_katebuild.cpp:209
#, kde-format
msgid "Build Selected Target"
msgstr "构建选中的目标"

#: plugin_katebuild.cpp:214
#, kde-format
msgid "Build and Run Selected Target"
msgstr "构建并运行选中的目标"

#: plugin_katebuild.cpp:219
#, kde-format
msgid "Stop"
msgstr "停止"

#: plugin_katebuild.cpp:224
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr "焦点移动到左侧的下一个标签页"

#: plugin_katebuild.cpp:244
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr "焦点移动到右侧的下一个标签页"

#: plugin_katebuild.cpp:266
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "目标设置"

#: plugin_katebuild.cpp:385
#, kde-format
msgid "Build Information"
msgstr "构建信息"

#: plugin_katebuild.cpp:466 plugin_katebuild.cpp:1223 plugin_katebuild.cpp:1234
#: plugin_katebuild.cpp:1255 plugin_katebuild.cpp:1265
#, kde-format
msgid "Project Plugin Targets"
msgstr "工程插件目标"

#: plugin_katebuild.cpp:563
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "没有指定要构建的文件或目录。"

#: plugin_katebuild.cpp:567
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr "“%1”不是一个本地文件，不能编译非本地的文件。"

#: plugin_katebuild.cpp:614
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"无法运行命令：%1\n"
"工作路径不存在：%2"

#: plugin_katebuild.cpp:628
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "运行“%1”失败。退出状态 = %2"

#: plugin_katebuild.cpp:643
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "<b>%1</b> 构建已取消"

#: plugin_katebuild.cpp:750
#, kde-format
msgid "No target available for building."
msgstr "没有可供编译的目标."

#: plugin_katebuild.cpp:764
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "没有指定要构建的本地文件或目录。"

#: plugin_katebuild.cpp:770
#, kde-format
msgid "Already building..."
msgstr "已经在构建..."

#: plugin_katebuild.cpp:797
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "构建目标 <b>%1</b>..."

#: plugin_katebuild.cpp:811
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Make 的结果：</title><nl/>%1"

#: plugin_katebuild.cpp:852
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:858
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "发现 %1 个错误。"

#: plugin_katebuild.cpp:862
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "发现 %1 个警告。"

#: plugin_katebuild.cpp:865
#, kde-format
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] ""

#: plugin_katebuild.cpp:870
#, kde-format
msgid "Build failed."
msgstr "构建失败。"

#: plugin_katebuild.cpp:872
#, kde-format
msgid "Build completed without problems."
msgstr "构建完成，没有出现问题。"

#: plugin_katebuild.cpp:877
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:901
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr "无法执行：%1 未设置工作目录。"

#: plugin_katebuild.cpp:1127
#, kde-format
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "错误"

#: plugin_katebuild.cpp:1130
#, kde-format
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "警告"

#: plugin_katebuild.cpp:1133
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1136
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "未定义的引用"

#: plugin_katebuild.cpp:1169
#, kde-format
msgid "Target Set"
msgstr "目标集"

#: plugin_katebuild.cpp:1171
#, kde-format
msgid "Clean"
msgstr "清理"

#: plugin_katebuild.cpp:1172
#, kde-format
msgid "Config"
msgstr "配置"

#: plugin_katebuild.cpp:1173
#, kde-format
msgid "ConfigClean"
msgstr "清理配置"

#: plugin_katebuild.cpp:1293
#, kde-format
msgid "build"
msgstr "构建"

#: plugin_katebuild.cpp:1296
#, kde-format
msgid "clean"
msgstr "清理"

#: plugin_katebuild.cpp:1299
#, kde-format
msgid "quick"
msgstr "快速"

#: TargetHtmlDelegate.cpp:47
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>目标:</B> %1"

#: TargetHtmlDelegate.cpp:49
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>目录:</B> %1"

#: TargetHtmlDelegate.cpp:98
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"留空使用当前文档的目录。\n"
"可添加搜索目录，以 ';' 分隔。"

#: TargetHtmlDelegate.cpp:102
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Use:\n"
"\"%f\" 为当前文件\n"
"\"%d\" 为当前文件的目录\n"
"\"%n\" 为不包括后缀的当前文件名"

#: TargetModel.cpp:388
#, kde-format
msgid "Command/Target-set Name"
msgstr "命令/目标集名称"

#: TargetModel.cpp:391
#, kde-format
msgid "Working Directory / Command"
msgstr "工作目录 / 命令"

#: TargetModel.cpp:394
#, kde-format
msgid "Run Command"
msgstr "运行命令"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr "筛选目标，使用方向键选择，回车键执行"

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "创建新的目标集"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "拷贝命令或目标集"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "删除当前目标或当前目标集"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "添加新目标"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "构建选中的目标"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr "构建并运行选中的目标"

#: targets.cpp:52
#, kde-format
msgid "Move selected target up"
msgstr "上移所选任务"

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr "下移所选任务"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "构建(&B)"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "插入路径"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "选择要插入的目录"
