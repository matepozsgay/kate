# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Federico Zenith <federico.zenith@member.fsf.org>, 2010, 2011, 2012, 2013, 2014, 2015.
# Paolo Zamponi <feus73@gmail.com>, 2018, 2020, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kategdbplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-15 00:49+0000\n"
"PO-Revision-Date: 2023-05-02 17:54+0200\n"
"Last-Translator: Paolo Zamponi <feus73@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.0\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "Comando GDB"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "..."

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Percorsi di ricerca dei file sorgente"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Applicazione locale"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "TCP remoto"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Porta seriale remota"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Host"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Porta"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "solib-search-path"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Comandi di inizializzazione personalizzati"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"È in corso una sessione di debug. Usa riesegui oppure interrompi la sessione "
"corrente."

#: configview.cpp:92
#, kde-format
msgid "Add new target"
msgstr "Aggiungi un nuovo obiettivo"

#: configview.cpp:96
#, kde-format
msgid "Copy target"
msgstr "Copia obiettivo"

#: configview.cpp:100
#, kde-format
msgid "Delete target"
msgstr "Elimina obiettivo"

#: configview.cpp:105
#, kde-format
msgid "Executable:"
msgstr "Eseguibile:"

#: configview.cpp:125
#, kde-format
msgid "Working Directory:"
msgstr "Cartella di lavoro:"

#: configview.cpp:133
#, kde-format
msgid "Process Id:"
msgstr "Identificativo di processo:"

#: configview.cpp:138
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argomenti:"

#: configview.cpp:141
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Mantieni attivazione"

#: configview.cpp:142
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Mantieni attiva la riga di comando"

#: configview.cpp:144
#, kde-format
msgid "Redirect IO"
msgstr "Ridireziona I/O"

#: configview.cpp:145
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Ridireziona l'I/O dei programmi sotto debug in una scheda a parte"

#: configview.cpp:147
#, kde-format
msgid "Advanced Settings"
msgstr "Impostazioni avanzate"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Obiettivi"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Obiettivo %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "Settings"
msgid "Settings File:"
msgstr "Impostazioni"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr ""

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, fuzzy, kde-format
#| msgid "Debug"
msgid "Debugger"
msgstr "Fai il debug"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Locali"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "Registri della CPU"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr "Imposta l'eseguibile nella scheda «Impostazioni» nel pannello «Debug»."

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"Nessun debugger selezionato. Selezionane uno nella scheda «Impostazioni» nel "
"pannello «Debug»."

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"Debugger non trovato. Assicurati che sia installato nel tuo sistema. Il "
"debugger selezionato è «%1»"

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "Impossibile avviare il processo di debug"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb concluso normalmente ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "tutti i thread sono in esecuzione"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr "thread in esecuzione: %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "fermato (%1)."

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Thread attivo: %1 (tutti i thread sono stati interrotti)."

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "Thread attivo: %1."

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Frame corrente: %1:%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Host: %1. Obiettivo: %1"

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: impossibile elaborare l'ultima risposta: %1. Troppi errori "
"consecutivi. Chiusura."

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: impossibile elaborare l'ultima risposta: %1"

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "Motore DAP non riuscito"

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "programma terminato"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "richiesta di disconnessione"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "richiesta di chiusura"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "Motore DAP: %1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Punti di interruzione raggiunti:"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "(continua) thread %1"

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "tutti i thread sono proseguiti"

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(in esecuzione)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** connessione al server chiusa ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "programma terminato con codice %1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** in attesa di azioni dell'utente ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "errore nella risposta: %1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "importante"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetria"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "debug del processo [%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "debug del processo %1"

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Metodo di avvio: %1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "thread %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "impostazione punto di interruzione"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "punto di interruzione eliminato"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "punto di interruzione (%1)"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<non valutato>"

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "caratteristiche del server"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "supportato"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "non supportato"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "punto di interruzione condizionale"

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "punti di interruzione nella funzione"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "raggiungi punti di interruzione condizionale"

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr "punti di registro"

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr "richieste dei moduli"

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "vai alle richieste degli obiettivi"

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr "richiesta di terminazione"

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "termina il debug"

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "errore di sintassi: espressione non trovata"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "errore di sintassi: %1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "riga non valida: %1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "file non specificato: %1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "identificativo del thread non valido: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "identificativo del thread non specificato: %1"

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Comandi disponibili:"

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "i punti di interruzione condizionali non sono supportati dal server"

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr ""
"i punti di interruzione condizionali di tipo hit non sono supportati dal "
"server"

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "la riga %1 ha già un punto di interruzione"

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "punto di interruzione non trovato (%1:%2)"

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Thread corrente: "

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr "nessuno"

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Frame corrente: "

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Stato della sessione: "

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "inizializzazione"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "in esecuzione"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "fermato"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "terminato"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "disconnesso"

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "post mortem"

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "comando non trovato"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "identificativo del thread mancante"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "terminazione del motore"

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Frame corrente [%3]: %1:%2 (%4)"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Simbolo"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Valore"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "tipo"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "elementi indicizzati"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "elementi con nome"

#: plugin_kategdb.cpp:106
#, kde-format
msgid "Kate Debug"
msgstr "Debug in Kate"

#: plugin_kategdb.cpp:110
#, kde-format
msgid "Debug View"
msgstr "Vista di debug"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:343
#, kde-format
msgid "Debug"
msgstr "Fai il debug"

#: plugin_kategdb.cpp:113 plugin_kategdb.cpp:116
#, kde-format
msgid "Locals and Stack"
msgstr "Locali e pila"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "Nº"

#: plugin_kategdb.cpp:168
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Frame"

#: plugin_kategdb.cpp:200
#, kde-format
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Risultato del debug"

#: plugin_kategdb.cpp:201
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Impostazioni"

#: plugin_kategdb.cpp:243
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Impossibile aprire il file:</title><nl/>%1<br/>Cerca di aggiungere un "
"percorso di ricerca in Impostazioni avanzate -> Percorsi di ricerca dei file "
"sorgente"

#: plugin_kategdb.cpp:268
#, kde-format
msgid "Start Debugging"
msgstr "Avvia il debug"

#: plugin_kategdb.cpp:278
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Termina / interrompi il debug"

#: plugin_kategdb.cpp:285
#, kde-format
msgid "Continue"
msgstr "Continua"

#: plugin_kategdb.cpp:291
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Commuta punto di interruzione / interrompi"

#: plugin_kategdb.cpp:297
#, kde-format
msgid "Step In"
msgstr "Entra"

#: plugin_kategdb.cpp:304
#, kde-format
msgid "Step Over"
msgstr "Passa oltre"

#: plugin_kategdb.cpp:311
#, kde-format
msgid "Step Out"
msgstr "Esci"

#: plugin_kategdb.cpp:318 plugin_kategdb.cpp:350
#, kde-format
msgid "Run To Cursor"
msgstr "Esegui fino al cursore"

#: plugin_kategdb.cpp:325
#, kde-format
msgid "Restart Debugging"
msgstr "Riavvia il debug"

#: plugin_kategdb.cpp:333 plugin_kategdb.cpp:352
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Sposta il contatore dei programmi"

#: plugin_kategdb.cpp:338
#, kde-format
msgid "Print Value"
msgstr "Stampa valore"

#: plugin_kategdb.cpp:347
#, kde-format
msgid "popup_breakpoint"
msgstr "comparsa_punto_interruzione"

#: plugin_kategdb.cpp:349
#, kde-format
msgid "popup_run_to_cursor"
msgstr "comparsa_esegui_al_cursore"

#: plugin_kategdb.cpp:431 plugin_kategdb.cpp:447
#, kde-format
msgid "Insert breakpoint"
msgstr "Inserisci punto di interruzione"

#: plugin_kategdb.cpp:445
#, kde-format
msgid "Remove breakpoint"
msgstr "Rimuovi punto di interruzione"

#: plugin_kategdb.cpp:599 plugin_kategdb.cpp:613
#, kde-format
msgid "Execution point"
msgstr "Punto di esecuzione"

#: plugin_kategdb.cpp:771
#, kde-format
msgid "Thread %1"
msgstr "Thread %1"

#: plugin_kategdb.cpp:871
#, kde-format
msgid "IO"
msgstr "I/O"

#: plugin_kategdb.cpp:956 plugin_kategdb.cpp:964
#, kde-format
msgid "Breakpoint"
msgstr "Punto di interruzione"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Debug"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "Debug Plugin"
msgstr "Estensione di debug"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Paolo Zamponi,Federico Zenith"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zapaolo@email.it,"

#~ msgid "GDB Integration"
#~ msgstr "Integrazione con GDB"

#~ msgid "Kate GDB Integration"
#~ msgstr "Integrazione di Kate con GDB"

#~ msgid "&Target:"
#~ msgstr "Obbie&ttivo:"

#~ msgctxt "Program argument list"
#~ msgid "&Arg List:"
#~ msgstr "Elenco di &argomenti:"

#~ msgid "Remove Argument List"
#~ msgstr "Rimuovi elenco di argomenti"

#~ msgid "Arg Lists"
#~ msgstr "Elenchi di argomenti"

#~ msgid "Add Working Directory"
#~ msgstr "Aggiungi cartella di lavoro"

#~ msgid "Remove Working Directory"
#~ msgstr "Rimuovi cartella di lavoro"
